/**
 * 
 */
package com.beats.app.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


/**
 * @author wapples
 *
 */
public class TrackListFragment extends ListFragment {	
	
	private static final String TRACK_NAME ="track_name";
	private static final String ARTIST_NAME = "artist_name";
	private static final String ARTISTS_LIST ="artist_list";
	private static final String ALBUM_NAME ="album_name";
	private static final String ALBUM_IMAGE ="album_image";
	private static final String TRACK_MAIN_GENRE ="track_main_genre";
	private static final String TRACK_DURATION ="duration";
		

	private LinearLayout mSearchBox;
	private ListView lv;
	private ActionBar bar;	
	private JSONObject content;
	private List<Map<String, Object>> dispListMap;
	private List<Map<String, Object>> docListMap;
	private String baseUrl = "";
	private ProgressDialog progressDialog;
	private EndlessMogSearchAdapater mogAdapter;
	FragmentManager fm;	
	Boolean docShowing = false;
	private Activity mActivity;
	private Integer mCurrentIndex = 0;
	private TrackListAdapter trackAdapter;
	private TextView mTrackCount;
	
	 /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    static TrackListFragment newInstance(Integer id, String tracks) {
    	Bundle extras = new Bundle();
    	extras.putInt("albumId", id);
    	extras.putString("trackCount", tracks);
    	TrackListFragment f = new TrackListFragment();
    	f.setArguments(extras);
        return f;
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		View v = inflater.inflate(R.layout.track_list_container, container, false);
		
		mActivity = getActivity();
		fm = getFragmentManager();
		progressDialog = new ProgressDialog(mActivity);
		progressDialog.setMessage("Loading Tracks, Please Wait...");
		progressDialog.setCancelable(true);
		dispListMap = new ArrayList<Map<String, Object>>();
		mTrackCount = (TextView) v.findViewById(R.id.track_count);

		Bundle extras = getArguments();
		Integer albumId = extras.getInt("albumId");
		String trackCount = extras.getString("trackCount");
		updateResults(trackCount);
		
		new GetAlbumDetails().execute(albumId);
		
		return v;
	}
	

	private class GetAlbumDetails extends AsyncTask<Integer, Void, JSONArray> {
		 Integer id;
	     protected JSONArray doInBackground(Integer... query) {
	 		 id = query[0];
	 		 return getAlbumDetails(id);
	     }
	     
	     protected void onPreExecute(){
	    	 progressDialog.show();
	     }
	     protected void onPostExecute(JSONArray tracks) {
	    	 if (tracks != null) {
	    			Integer len = tracks.length();
	    			List<Map<String, Object>> resultsMap = new ArrayList<Map<String,Object>>();
	    			for (int i = 0; i < len; i++) {
	    				Map<String, Object> map = new HashMap<String, Object>();

	    				try {
	    					JSONObject j = (JSONObject) tracks.get(i);
	    					
	    					//Get artists
	    					JSONArray artistsList = j.getJSONArray("primary_artists");
	    					String artistString = "";
	    	    			for (int g = 0; g < artistsList.length(); g++) {
	    	    				JSONObject artists = (JSONObject) artistsList.get(g);
	    	    				String artistName = U.gs(artists, "artist_name");
	    	    				if (g != 0){
		    	    				artistString += ", " + artistName;
	    	    				} else {
	    	    					artistString += artistName;
	    	    				}
	    	    			}
		
	    					String trackName = U.gs(j, "track_name");
	    					String mainGenre = U.gs(j, "main_genre");
	    					String artistName = U.gs(j, "artist_name");
	    					String albumImage = U.gs(j, "album_image");
	    					String albumName = U.gs(j, "album_name");
	    					
	    					//Duration conversion to h:m:s
	    					int duration = U.gi(j, "duration");
	    					String durationString = U.getTimeFromSeconds(duration);
	    					
	    					map.put(TRACK_NAME, trackName);
	    					map.put(ARTIST_NAME, artistName);
							map.put(TRACK_MAIN_GENRE, mainGenre);
							map.put(ARTISTS_LIST, artistString);
							map.put(ALBUM_IMAGE, albumImage);
							map.put(TRACK_DURATION, durationString);
							map.put(ALBUM_NAME, albumName);
							
	    					resultsMap.add(map);
	    					
						} catch (JSONException e) {
							e.printStackTrace();
						}	
	    			}

	    			//Create row list adapter
	    			trackAdapter = new TrackListAdapter(mActivity, resultsMap, baseUrl, fm, R.layout.track_list_view, 
						   new String[] {TRACK_NAME, ARTISTS_LIST, TRACK_MAIN_GENRE, ALBUM_IMAGE, TRACK_DURATION},
			               new int[]{ R.id.track_title_list, R.id.track_artists_list,  R.id.track_genre_list, R.id.track_avatar_list, R.id.track_duration_list});
					
	    			setListAdapter(trackAdapter);	
					progressDialog.dismiss();
		 		}
	     	}
	 }
	

	public JSONArray getAlbumDetails(Integer id) {
		MogApi mogApi = new MogApi();
		JSONArray validItems = new JSONArray();
		JSONArray bulkREsponse = null;
		JSONObject response = null;
		
		try {
			response = mogApi.getAlbumDetails(id);
			int returned = U.gi(response, "track_count");
			bulkREsponse = U.ga(response, "tracks");	
			for(int i = 0; i < returned; i++){
				validItems.put(bulkREsponse.get(i));
			}
		} catch (Exception e){
			
		}
		return validItems;	
	}
	 
	 public void updateResults(String i){
		 mTrackCount.setText(i.toString());
	 }
		
}
