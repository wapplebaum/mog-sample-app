package com.beats.app.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.SavedState;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class NewStartingActivity extends Activity {
	
	private FragmentManager fm;
	private AlbumListFragment clientFrag;
	private NewStartingActivity mContext;
	

	/** Called when the activity is first created. */
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mog_main);
		mContext = this;
		
		ActionBar bar = getActionBar();
		bar.setIcon(R.drawable.mog_nav);
		bar.setTitle("");

		// Get Fragment Manager
		fm = getFragmentManager();
		
		/**
		 * Spawn List Fragment
		 */
		clientFrag = new AlbumListFragment();
		showList();		
	}


	/** Apply Search bar to action bar using SearchView */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.layout.action_bar, menu);
		final SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
		searchView.setSubmitButtonEnabled(true);
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				return false;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				//AlbumListFragment list = (AlbumListFragment) fm.findFragmentByTag("listFrag");
				String q = searchView.getQuery().toString();
				if (!q.isEmpty()){
					clientFrag.siteSearch(q);
					Log.i("SEARCH", q);
				} else {
					Toast.makeText(mContext, "search can not be blank", Toast.LENGTH_SHORT).show();
				}		
				return false;
			}
		});
		
		return super.onCreateOptionsMenu(menu);
	}


	public void showList(){
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.add(R.id.fragment_content_wrapper, clientFrag, "listFrag");
		ft.commit();
	}

}
