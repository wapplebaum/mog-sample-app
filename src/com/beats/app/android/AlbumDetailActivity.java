package com.beats.app.android;


import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author wapples
 */
public class AlbumDetailActivity extends Activity {
		
	private String mArtistName;
	private String mAlbumName;
	private String mAlbumImage;
	private String mMainGenre;
	private String mDuration;
	private String mTrackCount;
	private Integer mYear;
	private Bitmap defaultIcon;
	private ImageDownloader imageDownloader;
	private ProgressDialog progressDialog;
	private Integer mAlbumId;
	private String mAlbumLabel;

	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.album_details);
		
		ActionBar bar = getActionBar();
		bar.setIcon(R.drawable.mog_nav);
		
        defaultIcon = BitmapFactory.decodeResource(getResources(),
                R.drawable.mog_mobile_icon);
        imageDownloader = new ImageDownloader(defaultIcon);
        
        //set up progress dialog
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage("Loading Results, Please Wait...");
		progressDialog.setCancelable(true);
	    
		Bundle extras = getIntent().getExtras();
		mArtistName = extras != null ? extras.getString("artistName") : null;
		mAlbumName = extras != null ? extras.getString("albumName") : null;
		mAlbumImage = extras != null ? extras.getString("albumImage") : null;
		mMainGenre = extras != null ? extras.getString("mainGenre") : null;
		mDuration = extras != null ? extras.getString("duration") : null;
		mAlbumLabel = extras != null ? extras.getString("albumLabel") : null;
		mTrackCount = extras != null ? extras.getString("trackCount") : null;
		mYear = extras != null ? extras.getInt("year") : null;
		mAlbumId = extras != null ? extras.getInt("albumId") : null;

		//Set activity title to artist - album title
		setTitle(mArtistName + " - " + mAlbumName);

		ImageView albumAvatar = (ImageView) findViewById(R.id.album_avatar_details);
		TextView albumTitle = (TextView) findViewById(R.id.album_title_details);
		TextView artistName = (TextView) findViewById(R.id.artist_name_details);
		TextView albumDuration = (TextView) findViewById(R.id.album_duration_details);
		TextView albumGenre = (TextView) findViewById(R.id.album_genre_details);
		TextView albumYear = (TextView) findViewById(R.id.album_year_details);

		imageDownloader.download(mAlbumImage, albumAvatar);
		albumTitle.setText(mAlbumName);
		artistName.setText(mArtistName);
		albumDuration.setText(mDuration);
		albumGenre.setText(mMainGenre);
		albumYear.setText(mYear.toString());
		
		//Add list fragment to activity
		addTrackList();
	}
	
	
	//Query mog api for album details
	public JSONArray getAlbumDetails(Integer id) {
		MogApi mogApi = new MogApi();
		JSONArray validItems = new JSONArray();
		JSONArray bulkREsponse = null;
		JSONObject response = null;
		
		try {
			response = mogApi.getAlbumDetails(id);
			int returned = U.gi(response, "track_count");
			bulkREsponse = U.ga(response, "tracks");	
			for(int i = 0; i < returned; i++){
				validItems.put(bulkREsponse.get(i));
			}
		} catch (Exception e){
			
		}
		return validItems;	
	}
	
	//Function to add track list fragment
	public void addTrackList(){
		TrackListFragment tf = TrackListFragment.newInstance(mAlbumId, mTrackCount);
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.track_list_frame, tf, "trackFrag");
		ft.commit();
	}

	

}
