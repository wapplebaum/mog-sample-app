package com.beats.app.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

public class U {
	    
	/**
	 * Safely get integer value i o[key].
	 * 
	 * @param o
	 * @param key
	 * @return
	 */
	public static Integer gi(JSONObject o, String key) {
		Integer i;
		try {
			i = o.getInt(key);
		}
		catch (JSONException e) {
			i = 0;
		}
		return i;
	}

	/**
	 * Safely get integer value i o[key].
	 * 
	 * @param o
	 * @param key
	 * @return
	 */
	public static Double gd(JSONObject o, String key) {
		Double d;
		try {
			d = o.getDouble(key);
		}
		catch (JSONException e) {
			d = 0.0;
		}
		return d;
	}
	
	/**
	 * Safely get string value in o[key].
	 * 
	 * @param o
	 * @param key
	 * @return
	 */
	public static String gs(JSONObject o, String key) {
		String s;
		try {
			s = o.getString(key);
		}
		catch (JSONException e) {
			s = "";
		}
		return s;
	}

	/**
	 * Safely get boolean value in o[key].
	 * 
	 * @param o
	 * @param key
	 * @return
	 */
	public static boolean gb(JSONObject o, String key) {
		boolean b;
		try {
			b = o.getBoolean(key);
		}
		catch (JSONException e) {
			b = false;
		}
		return b;
	}
	/**
	 * Safely get JSONArray in o[key]
	 * 
	 * @param o
	 * @param key
	 * @return
	 */
	public static JSONArray ga(JSONObject o, String key) {
		JSONArray ja;
		try {
			ja = o.getJSONArray(key);
		}
		catch (Exception e) {
			ja = new JSONArray();
		}
		return ja;
	}
	
	
	
	/**
	 * Safely get JSONObject in o[key]
	 * 
	 * @param o
	 * @param key
	 * @return
	 */
	public static JSONObject go(JSONObject o, String key) {
		JSONObject jo;
		try {
			jo = o.getJSONObject(key);
		}
		catch (JSONException e) {
			jo = new JSONObject();
		}
		return jo;
	}
	

	
	public static void clearImage(ImageView v){
		v.setVisibility(View.GONE);
	}
	
	public static void setImage(ImageView v, int resid){
		if (v.getVisibility() != View.VISIBLE){
			v.setVisibility(View.VISIBLE);
		}
		v.setBackgroundResource(resid);
	}
	
	public static String getTimeFromSeconds(Integer totalSeconds){
		String timeString = "";
		if (totalSeconds > 0) {
			Integer hours = totalSeconds / 3600;
			Integer minutes = (totalSeconds % 3600) / 60;
			Integer seconds = totalSeconds % 60;
			String sSeconds = seconds.toString();
			String sMinutes = minutes.toString();
		
			if (seconds < 10 && seconds > 0) {
				sSeconds = "0"+sSeconds;
			}
			if (minutes < 10 && minutes > 0) {
				sMinutes = "0"+sMinutes;
			}
			timeString = hours + ":" + sMinutes + ":" + sSeconds;
		} 
		return timeString;
	}
	
	
}
