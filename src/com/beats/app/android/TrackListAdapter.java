package com.beats.app.android;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.sax.StartElementListener;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.android.maps.GeoPoint;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class TrackListAdapter extends BaseAdapter implements Filterable {
    private int[] mTo;
    private String[] mFrom;
    private ViewBinder mViewBinder;
    private List<Map<String, Object>> mData;
    private int mResource;
    private int mDropDownResource;
    private LayoutInflater mInflater;
    private ArrayList<Map<String, ?>> mUnfilteredData;
	private Context mContext;
	private Typeface mediumAvenir;
	private Typeface heavyAvenir;
	private ImageDownloader imageDownloader;
	private Bitmap defaultIcon;
	private String mQuery;
	private FragmentManager mFm;
	private Bundle extras;
	private String trackName;
	private String artistName;
	private String albumName;
	private String albumImage;
    
    public TrackListAdapter(Context context, List<Map<String, Object>> data, String baseUrl,FragmentManager manager,
            int resource, String[] from, int[] to) {
        mData = data;
        mResource = mDropDownResource = resource;
        mFrom = from;
        mTo = to;
        mContext = context;
        mFm = manager;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        defaultIcon = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.mog_mobile_icon);

        imageDownloader = new ImageDownloader(defaultIcon);

		AssetManager assets = context.getAssets();
		mediumAvenir = Typeface.createFromAsset(assets, "fonts/AvenirLTStd-Medium.otf");
		heavyAvenir = Typeface.createFromAsset(assets, "fonts/AvenirLTStd-Heavy.otf");
		 
    }

    
    /**
     * @see android.widget.Adapter#getCount()
     */
    public int getCount() {
        return mData!=null ? mData.size() : 0; 
    }

    /**
     * @see android.widget.Adapter#getItem(int)
     */
    public Object getItem(int position) {
        return mData != null ? mData.get(position) : null;
    }

    /**
     * @see android.widget.Adapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * @see android.widget.Adapter#getView(int, View, ViewGroup)
     */
    public View getView(int position, View convertView, ViewGroup parent) {    	
        return createViewFromResource(position, convertView, parent, mResource);
    }
    

    private View createViewFromResource(int position, View convertView,
            ViewGroup parent, int resource) {
    	final Integer listPosition = position;
        View v;
        if (convertView == null) {
            v = mInflater.inflate(resource, parent, false);
        } else {
            v = convertView;
        }
       
        v.setFocusable(true);

        TextView trackNameText = (TextView) v.findViewById(R.id.track_title_list);        
        TextView durationText = (TextView) v.findViewById(R.id.track_duration_list);
        TextView artistText = (TextView) v.findViewById(R.id.track_artists_list);
        
        trackNameText.setTypeface(heavyAvenir);
        durationText.setTypeface(mediumAvenir);
        
        
		artistName = (String) mData.get(listPosition).get("artist_name");
		albumName = (String) mData.get(listPosition).get("album_name");
		albumImage = (String) mData.get(listPosition).get("album_image");
		trackName = (String) mData.get(listPosition).get("track_name");
		
		extras = new Bundle();
		extras.putString("artist_Name", artistName);
		extras.putString("track_name", trackName);
		extras.putString("album_name", albumName);
		extras.putString("album_image", albumImage);
		
		 v.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					
					//Use AlertDialog instead of dialog fragment for now
					//Instantiate an AlertDialog.Builder with its constructor
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					View dialogLayout = mInflater.inflate(R.layout.track_dialog, null);
					builder.setView(dialogLayout);
					
			        RelativeLayout watchContainer = (RelativeLayout) dialogLayout.findViewById(R.id.watch);
			        RelativeLayout buyContainer = (RelativeLayout) dialogLayout.findViewById(R.id.buy);
			        RelativeLayout shareContainer = (RelativeLayout) dialogLayout.findViewById(R.id.share);
					
			        /**
			         * Track menu item handlers,
			         */
			        watchContainer.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            	
			            	String watchUrl = "http://www.youtube.com/results?search_query=" + artistName + "+" + trackName;
			            	Intent i = new Intent(Intent.ACTION_VIEW, Uri
			           				.parse(watchUrl));
			            	mContext.startActivity(i);
			            }
			        });
			        
			        buyContainer.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            	
			            	String buyUrl = "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Ddigital-music&" +
			            			"field-keywords="+trackName + "+" + artistName;
			            	Intent i = new Intent(Intent.ACTION_VIEW, Uri
			           				.parse(buyUrl));
			            	mContext.startActivity(i);
			            }
			        });
			        
			        //Submit a message string to Action_Send intent
			        shareContainer.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            	String message = trackName + " by: " + artistName;
							Intent shareIntent = new Intent(Intent.ACTION_SEND);
							shareIntent.putExtra(Intent.EXTRA_TEXT, message); 
							shareIntent.setType("text/plain");   
							mContext.startActivity(Intent.createChooser(shareIntent, "Share"));
			            }
			        });

					AlertDialog dialog = builder.create();
					dialog.show();

					/* DialogFragment newFragment = new TrackMenuDialogFragment();
					 newFragment.setArguments(extras);
					 newFragment.show(mFm, "trackDialog");*/					
				}
		 });

        bindView(position, v);
        return v;
    }

    /**
     * <p>Sets the layout resource to create the drop down views.</p>
     *
     * @param resource the layout resource defining the drop down views
     * @see #getDropDownView(int, android.view.View, android.view.ViewGroup)
     */
    public void setDropDownViewResource(int resource) {
        this.mDropDownResource = resource;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return createViewFromResource(position, convertView, parent, mDropDownResource);
    }

    private void bindView(int position, View view) {
        final Map dataSet = mData.get(position);
        if (dataSet == null) {
            return;
        }
        
        final String[] from = mFrom;
        final int[] to = mTo;
        final int len = to.length;
        //Bitmap icon= BitmapFactory.decodeResource(mContext.getResources(), R.drawable.wm_new_thumb);

        
        for (int i = 0; i < len; i++) {
            final View v = view.findViewById(to[i]);
            //final RatingBar rb = (RatingBar) view.findViewById(R.id.dispensary_rating_list);
            if (v != null) {
                final Object data = dataSet.get(from[i]);
                String text = data == null ? "" : data.toString();
            	//Log.i("DATA", data.toString()); 

                if (text == null) {
                    text = "";
                }

                boolean bound = false;
                if (mViewBinder != null) {
                    bound = mViewBinder.setViewValue(v, data, text);
                }

                if (!bound) {
                    if (v instanceof TextView) {
                        setViewText((TextView) v, text);
                    } 
                    	else if (v instanceof ImageView) {
                        	imageDownloader.download(data.toString(), (ImageView) v);

                    		//if (v.getId() == R.id.track_avatar_list){
                    		//}
                     	}
                    	else if (v.getClass() == RatingBar.class) {
                    		Double ratingValue = (Double) data;
                    		setViewRating((RatingBar) v, ratingValue);
                    	} 
                    else {
                        throw new IllegalStateException(v.getClass().getName() + " is not a " +
                                " view that can be bounds by this SimpleAdapter");
                    }
                }
            }
        }
    }

    /**
     * Returns the {@link ViewBinder} used to bind data to views.
     *
     * @return a ViewBinder or null if the binder does not exist
     *
     * @see #setViewBinder(android.widget.SimpleAdapter.ViewBinder)
     */
    public ViewBinder getViewBinder() {
        return mViewBinder;
    }

    /**
     * Sets the binder used to bind data to views.
     *
     * @param viewBinder the binder used to bind data to views, can be null to
     *        remove the existing binder
     *
     * @see #getViewBinder()
     */
    public void setViewBinder(ViewBinder viewBinder) {
        mViewBinder = viewBinder;
    }

    /**
     * Called by bindView() to set the image for an ImageView but only if
     * there is no existing ViewBinder or if the existing ViewBinder cannot
     * handle binding to an ImageView.
     *
     * This method is called instead of {@link #setViewImage(ImageView, String)}
     * if the supplied data is an int or Integer.
     *
     * @param v ImageView to receive an image
     * @param value the value retrieved from the data set
     *
     * @see #setViewImage(ImageView, String)
     */
    public void setViewImage(ImageView v, int value) {
        v.setImageResource(value);
    }

    /**
     * Called by bindView() to set the image for an ImageView but only if
     * there is no existing ViewBinder or if the existing ViewBinder cannot
     * handle binding to an ImageView.
     *
     * By default, the value will be treated as an image resource. If the
     * value cannot be used as an image resource, the value is used as an
     * image Uri.
     *
     * This method is called instead of {@link #setViewImage(ImageView, int)}
     * if the supplied data is not an int or Integer.
     *
     * @param v ImageView to receive an image
     * @param value the value retrieved from the data set
     *
     * @see #setViewImage(ImageView, int) 
     */
    public void setViewImage(ImageView v, String value) {
        try {
            v.setImageResource(Integer.parseInt(value));
        } catch (NumberFormatException nfe) {
            v.setImageURI(Uri.parse(value));
        }
    }
    public void setViewRating(RatingBar v, Double value) {
        try {
            v.setRating(value.floatValue());
        } catch (NumberFormatException nfe) {
            
        }
    }
    

    /**
     * Called by bindView() to set the text for a TextView but only if
     * there is no existing ViewBinder or if the existing ViewBinder cannot
     * handle binding to an TextView.
     *
     * @param v TextView to receive text
     * @param text the text to be set for the TextView
     */
    public void setViewText(TextView v, String text) {
        v.setText(text);
    }
    
    //Add an album record to the data set
    public void add(Map<String, Object> map){
    	mData.add(map);
    	notifyDataSetChanged();
    }

    /**
     * This class can be used by external clients of SimpleAdapter to bind
     * values to views.
     *
     * You should use this class to bind values to views that are not
     * directly supported by SimpleAdapter or to change the way binding
     * occurs for views supported by SimpleAdapter.
     *
     * @see SimpleAdapter#setViewImage(ImageView, int)
     * @see SimpleAdapter#setViewImage(ImageView, String)
     * @see SimpleAdapter#setViewText(TextView, String)
     */
    public static interface ViewBinder {
        /**
         * Binds the specified data to the specified view.
         *
         * When binding is handled by this ViewBinder, this method must return true.
         * If this method returns false, SimpleAdapter will attempts to handle
         * the binding on its own.
         *
         * @param view the view to bind the data to
         * @param data the data to bind to the view
         * @param textRepresentation a safe String representation of the supplied data:
         *        it is either the result of data.toString() or an empty String but it
         *        is never null
         *
         * @return true if the data was bound to the view, false otherwise
         */
        boolean setViewValue(View view, Object data, String textRepresentation);
    }

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return null;
	}

    
   
}
