package com.beats.app.android;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class TrackMenuDialogFragment extends DialogFragment {

	private String trackName;
	private String artistName;
	private String albumName;
	private String albumImage;

	public static DialogFragment newInstance(int mStackLevel) {
		
		return null;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(true);
        int style = DialogFragment.STYLE_NORMAL;
        int theme = android.R.style.Theme_Holo;
        setStyle(style, theme);
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
        View v = inflater.inflate(R.layout.track_dialog, container, false);
        
        //Get data from extras
        
        Bundle extras = getArguments();
        trackName = extras.getString("track_name");
        artistName = extras.getString("artist_name");
        albumName = extras.getString("album_name");
        albumImage = extras.getString("album_image");
   
        RelativeLayout watchContainer = (RelativeLayout) v.findViewById(R.id.watch);
        RelativeLayout buyContainer = (RelativeLayout) v.findViewById(R.id.buy);
        RelativeLayout shareContainer = (RelativeLayout) v.findViewById(R.id.share);

        // Click handlers for Items.
        
        /**
         * Watch button handler, 
         * takes the track name and artist and submits it to the youtube/search intent
         */
        watchContainer.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	
            	String tvUrl = "http://www.youtube.com/results?search_query=" + artistName + "+" + trackName;
            	Intent i = new Intent(Intent.ACTION_VIEW, Uri
           				.parse(tvUrl));
            }
        });
        
       
        return v;
    }
    

    
}