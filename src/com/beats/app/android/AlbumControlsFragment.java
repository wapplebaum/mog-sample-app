/**
 * 
 */
package com.beats.app.android;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * @author wapples
 *
 */
public class AlbumControlsFragment extends Fragment {	

	private ProgressDialog progressDialog;
	FragmentManager fm;	
	private Activity mActivity;
	public TrackListAdapter trackAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		View v = inflater.inflate(R.layout.album_controls, container, false);
		
		mActivity = getActivity();
		progressDialog = new ProgressDialog(mActivity);
		progressDialog.setMessage("Loading Results, Please Wait...");
		progressDialog.setCancelable(true);

		return v;
	}
	
	
}
