/**
 * 
 */
package com.beats.app.android;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * @author wapples
 *
 */
public class AlbumListFragment extends ListFragment {	
	
	private static final String ARTIST_ID ="artist_id";
	private static final String ARTIST_NAME = "artist_name";
	private static final String ALBUM_ID ="album_id";
	private static final String ALBUM_NAME ="album_name";
	private static final String ALBUM_IMAGE ="album_image";
	private static final String ALBUM_MAIN_GENRE ="main_genre";
	private static final String ALBUM_DURATION ="duration";
	private static final String ALBUM_LABEL ="label";
	private static final String ALBUM_TRACK_COUNT ="track_count";
	private static final String ALBUM_YEAR ="year";
	private static final String ALBUM_POPULARITY ="popularity";

	private String baseUrl = "";
	private ProgressDialog progressDialog;
	private EndlessMogSearchAdapater mogAdapter;
	FragmentManager fm;	
	Boolean docShowing = false;
	private Activity mActivity;
	private TextView mSearchResultView;
	private Integer mCurrentIndex = 0;
	private TextView mClearButton;
	
	
	 /**
     * Create a new instance of CountingFragment,
     *
     */
    static AlbumListFragment newInstance() {
    	AlbumListFragment f = new AlbumListFragment();
        return f;
    }
	
    
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View v = inflater.inflate(R.layout.album_list_container, container, false);
		
		mActivity = getActivity();
		fm = getFragmentManager();
		
		progressDialog = new ProgressDialog(mActivity);
		progressDialog.setMessage("Loading Results, Please Wait...");
		progressDialog.setCancelable(true);
		
		mSearchResultView = (TextView) v.findViewById(R.id.search_results);
		mClearButton = (TextView) v.findViewById(R.id.clear_list);
		mClearButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				clearList();
		}});
		
		return v;	
	}
	
	
	public void sortByFeatured(List<Map<String, Object>> dataMap){		
		// Comparator by featured
		Comparator<Map<String, Object>> featuredSort = new Comparator<Map<String, Object>>() {
			public int compare(Map<String, Object> obj1,Map<String, Object> obj2) {
			    return ((Integer) obj1.get(ALBUM_DURATION)).compareTo((Integer)obj2.get(ALBUM_DURATION));
			}
		};		 
		 
		Collections.sort(dataMap, featuredSort);
	}
	
	public void sortByPopularity(List<Map<String, Object>> dataMap){		
		// Comparator by rating
		Comparator<Map<String, Object>> ratingSort = new Comparator<Map<String, Object>>() {
			public int compare(Map<String, Object> obj1,Map<String, Object> obj2) {
			    return ((Double) obj1.get(ALBUM_POPULARITY)).compareTo((Double)obj2.get(ALBUM_POPULARITY));
			}
		};	 
		 
		Collections.sort(dataMap, ratingSort);
	}
	
	/**
	 * siteSearch Takes a query string and fires off an async task to retrieve the results
	 * @param q
	 */
	public void siteSearch(String q){
		new GetMogSearch().execute(q);
	}
	
	/**
	 * @param 
	 */
	 private class GetMogSearch extends AsyncTask<String, Void, JSONArray> {
		 String q;
	     protected JSONArray doInBackground(String... query) {
	 		 q = query[0];
	    	 Log.i("SEARCH", q);
	    	 
	    	 //set results to 0
	    	 mCurrentIndex = 0;
	    	 
	    	 return getSearchResponse(q);
	     }
	     
	     protected void onPreExecute(){
	    	 progressDialog.show();
	     }
	     protected void onPostExecute(JSONArray albums) {
	    	 if (albums != null) {
		 			
	    			List<Map<String, Object>> resultsMap = new ArrayList<Map<String,Object>>();
	    			Integer len = albums.length();
	    			mSearchResultView.setText(len.toString());

	    			for (int i = 0; i < len; i++) {
	    				Map<String, Object> map = new HashMap<String, Object>();
	    				try {
	    					JSONObject j = (JSONObject) albums.get(i);
	    					
	    					int artist_id = U.gi(j, "artist_id");
	    					String artist_name = U.gs(j, "artist_name");
	    					int album_id = U.gi(j, "album_id");
	    					String album_name = U.gs(j, "album_name");
	    					String album_image = U.gs(j, "album_image");
	    					String main_genre = U.gs(j, "main_genre");
	    					String label = U.gs(j, "label");
	    					int track_count = U.gi(j, "track_count");
	    					int year = U.gi(j, "year");
	    					Double popularity = U.gd(j, "popularity");
	    					
	    					//Duration conversion to h:m:s
	    					int duration = U.gi(j, "duration");
	    					String durationString = U.getTimeFromSeconds(duration);

	    					map.put(ARTIST_ID, artist_id);
	    					map.put(ARTIST_NAME, artist_name);
							map.put(ALBUM_ID, album_id);
							map.put(ALBUM_NAME, album_name);
							map.put(ALBUM_IMAGE, album_image);
							map.put(ALBUM_LABEL, label);
							map.put(ALBUM_YEAR, year);
							map.put(ALBUM_MAIN_GENRE, main_genre);
							map.put(ALBUM_DURATION, durationString);
							map.put(ALBUM_POPULARITY, popularity);
							map.put(ALBUM_TRACK_COUNT, track_count + " Tracks");
							
	    					resultsMap.add(map);
						} catch (JSONException e) {
							e.printStackTrace();
						}	
	    			}
		 			
	    			sortByPopularity(resultsMap);
	    			
		 			//Create row list adapter
	    			mogAdapter = new EndlessMogSearchAdapater(getActivity(), fm, resultsMap, baseUrl,R.layout.album_list_view, 
						   new String[] {ALBUM_NAME, ALBUM_IMAGE, ARTIST_NAME, ALBUM_DURATION, ALBUM_YEAR, ALBUM_TRACK_COUNT, ALBUM_MAIN_GENRE},
			               new int[]{ R.id.album_title_list, R.id.album_avatar_list, R.id.artist_name_list, R.id.album_duration_list, R.id.album_year_list, R.id.album_tracks_list, R.id.album_genre_list},
	    					q);
					
					setListAdapter(mogAdapter);	
					progressDialog.dismiss();
		 		}
	     	}
	 }
	
	 /**
	  * updateResuts updates the track count text view
	  * @param i
	  */
	 public void updateResults(Integer i){
		 mCurrentIndex = i;
		 mSearchResultView.setText(i.toString());
	 }
	 
	 //function to clear the list
	 public void clearList(){
		 setListAdapter(null);
		 updateResults(0);
	 }
	 
		public JSONArray getSearchResponse(String q) {
			MogApi mogApi = new MogApi();
			JSONArray validItems = new JSONArray();
			JSONArray bulkREsponse = null;
			JSONObject response = null;	
			try {
				response = mogApi.getMogSearch(q, mCurrentIndex, 20);
				int returned = U.gi(response, "count");
				bulkREsponse = U.ga(response, "albums");	
				for(int i = 0; i < returned; i++){
					validItems.put(bulkREsponse.get(i));
				}
			} catch (Exception e){
				
			}
			
			return validItems;	
		}
}
