package com.beats.app.android;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import com.commonsware.cwac.endless.EndlessAdapter;

public class EndlessMogSearchAdapater extends EndlessAdapter{
    

    private List<? extends Map<String, ?>> mData;
	private Context mContext;
	private RotateAnimation rotate=null;
	private View pendingView=null;
	AlbumListFragment listFrag;
	private String mQuery;
	AlbumListAdapter mMogAdapter;
	JSONArray mNewData;
	
	private static final String ARTIST_ID ="artist_id";
	private static final String ARTIST_NAME = "artist_name";
	private static final String ALBUM_ID ="album_id";
	private static final String ALBUM_NAME ="album_name";
	private static final String ALBUM_IMAGE ="album_image";
	private static final String ALBUM_MAIN_GENRE ="main_genre";
	private static final String ALBUM_DURATION ="duration";
	private static final String ALBUM_LABEL ="label";
	private static final String ALBUM_TRACK_COUNT ="track_count";
	private static final String ALBUM_YEAR ="year";
	private static final String ALBUM_POPULARITY ="popularity";
	AlbumListFragment albumListFragment;
	private FragmentManager mFm;


    
    public EndlessMogSearchAdapater(Context context,FragmentManager manager, List<Map<String, Object>> data, String baseUrl,
            int resource, String[] from, int[] to, String query) {
    	super(new AlbumListAdapter(context, manager, data, baseUrl, resource, from, to, query));
    	
        mData = data;
        mContext = context;
        mQuery = query;
        mFm = manager;
        albumListFragment = (AlbumListFragment) mFm.findFragmentByTag("listFrag");
        //albumListFragment = ClientSearchFragment.newInstance();
        mMogAdapter = (AlbumListAdapter) getWrappedAdapter();
        rotate=new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        rotate.setDuration(600);
        rotate.setRepeatMode(Animation.RESTART);
        rotate.setRepeatCount(Animation.INFINITE);
    	
    }
    
    /**
     * called when endless row is shown
     */
    @Override
    protected View getPendingView(ViewGroup parent) {
      View row=LayoutInflater.from(parent.getContext()).inflate(R.layout.row, null);
      
      pendingView=row.findViewById(android.R.id.text1);
      pendingView.setVisibility(View.GONE);
      pendingView=row.findViewById(R.id.throbber);
      pendingView.setVisibility(View.VISIBLE);
      startProgressAnimation();
      
      return(row);
    }
    
    // get next 20 records from api
	@Override
	protected boolean cacheInBackground() throws Exception {
		boolean results = false;
		mNewData = albumListFragment.getSearchResponse(mQuery);
		if (mNewData.length() > 0){
			results = true;
		}
		return results;
	}
	
	
	/**
	 * Process next 20 records 
	 * For each one, call adapater.add(map)
	 */
	
	@Override
	protected void appendCachedData() {
		for(int i = 0; i < mNewData.length(); i++){
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				JSONObject j = (JSONObject) mNewData.get(i);
				
				int artist_id = U.gi(j, "artist_id");
				String artist_name = U.gs(j, "artist_name");
				int album_id = U.gi(j, "album_id");
				String album_name = U.gs(j, "album_name");
				String album_image = U.gs(j, "album_image");
				String main_genre = U.gs(j, "main_genre");
				String label = U.gs(j, "label");
				int track_count = U.gi(j, "track_count");
				int year = U.gi(j, "year");
				Double popularity = U.gd(j, "popularity");
				
				//Duration conversion to h:m:s
				int duration = U.gi(j, "duration");
				String durationString = U.getTimeFromSeconds(duration);

				map.put(ARTIST_ID, artist_id);
				map.put(ARTIST_NAME, artist_name);
				map.put(ALBUM_ID, album_id);
				map.put(ALBUM_NAME, album_name);
				map.put(ALBUM_IMAGE, album_image);
				map.put(ALBUM_LABEL, label);
				map.put(ALBUM_YEAR, year);
				map.put(ALBUM_MAIN_GENRE, main_genre);
				map.put(ALBUM_DURATION, durationString);
				map.put(ALBUM_POPULARITY, popularity);
				map.put(ALBUM_TRACK_COUNT, track_count + " Tracks");
				
				mMogAdapter.add(map);
			} catch (JSONException e) {
				e.printStackTrace();
			}	
			
			//Update list fragment's returned results value
			Integer adapterLen = mMogAdapter.getCount();
			albumListFragment.updateResults(adapterLen);
		}
	}


	 public void startProgressAnimation() {
		    if (pendingView!=null) {
		      pendingView.startAnimation(rotate);
		    }
	 }
}