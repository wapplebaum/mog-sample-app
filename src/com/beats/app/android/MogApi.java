package com.beats.app.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.maps.GeoPoint;

public class MogApi {
	
	private String mBaseUrl = "http://api.mog.com/v2/";
	private String mPostUrl = "";
	private Object StringBuilder;

	
	/**
	 * Read all data from an InputStream and return it as a String.
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static String slurp (InputStream in) throws IOException {
		BufferedReader r = new BufferedReader(new InputStreamReader(in));
		StringBuilder total = new StringBuilder();
		String line;
		while ((line = r.readLine()) != null) {
		    total.append(line);
		}
		/*
	    StringBuffer out = new StringBuffer();
	    byte[] b = new byte[4096];
	    for (int n; (n = in.read(b)) != -1;) {
	        out.append(new String(b, 0, n));
	    }
	    return out.toString();*/
		return total.toString();
	}

	/**
	 * Make an HTTP GET request to the given URL and return a JSONObject.
	 * @param url
	 * @return JSONObject
	 */
	public JSONObject getObject(URL url) {
		InputStream contentStream;
		String contentString;
		JSONObject j = null;
		//Log.i("NodeApi", url.toString());

		try {
			contentStream = (InputStream) url.getContent();
			if (contentStream != null) {
				contentString = slurp(contentStream);
				if (contentString != null) {
					j = new JSONObject(contentString);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return j;
	}

	public JSONArray getArray(URL url) {
		InputStream contentStream;
		String contentString;
		JSONArray j = null;
		boolean isBadResponse = false;
		try {
			contentStream = (InputStream) url.getContent();
			if (contentStream != null) {
				contentString = slurp(contentStream);
				if (contentString != null) {
					j = new JSONArray(contentString);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			isBadResponse = true;
		} catch (JSONException e) {
			e.printStackTrace();
			isBadResponse = true;
		} catch (NullPointerException e) {
			e.printStackTrace();
			isBadResponse = true;
		}
		
		// If we can't get data back, this will prevent code that uses
		// this method from blowing up.
		if (isBadResponse) {
			j = new JSONArray();
		}
		return j;
	}
	
	
	public JSONObject getMogSearch(String q, Integer index, Integer count){
		URL reqUrl = null;
		JSONObject response = null;
		try{
			if (q != null){
				StringBuilder query = new StringBuilder();
				query.append(mBaseUrl);
				query.append("albums/search.json?q=");
				query.append(URLEncoder.encode(q, "UTF-8"));
				query.append("&index=" + index);
				query.append("&count="+count);
				
				reqUrl = new URL(query.toString());
				Log.i("SEARCH", reqUrl.toString());
			}
			response =  this.getObject(reqUrl);
		} 	catch(UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch(MalformedURLException e) {
			e.printStackTrace();

		}
		
		return response;
	}
	
	public JSONObject getAlbumDetails(Integer id){
		URL reqUrl = null;
		JSONObject response = null;
		try{
			reqUrl = new URL(mBaseUrl + "/albums/"+id+".json");
			Log.i("SEARCH", reqUrl.toString());
			response =  this.getObject(reqUrl);
		} 	catch(MalformedURLException e) {
			e.printStackTrace();
		}
		return response;
	}

}
