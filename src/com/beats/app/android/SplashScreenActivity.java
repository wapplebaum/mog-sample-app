
package com.beats.app.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;
 
public class SplashScreenActivity extends Activity {
	
	private ProgressDialog mProgressDialog;

	 
	/** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.splash);
  	  final SplashScreenActivity self = this;
  	  
  	  //Create timed thread to show splash screen and then launch main activity
  	  Thread splashThread = new Thread() {
         @Override
         public void run() {
            try {	
               int waited = 0;
               while (waited < 1500) {
                  sleep(100);
                  waited += 100;
               }
               
            } catch (Exception e) {
            } finally { 
            	Intent i = new Intent(self, NewStartingActivity.class);
            	startActivity(i);
                SplashScreenActivity.this.finish();
            }
         }
      };
      
      splashThread.start();
   }
}